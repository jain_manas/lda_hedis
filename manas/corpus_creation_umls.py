if True:
    from pyspark.sql.functions import col
    from pyspark.sql.functions import regexp_replace
    from pyspark.ml.feature import Tokenizer
    from pyspark.ml.feature import StopWordsRemover
    from pyspark.sql.functions import udf
    from pyspark.sql.functions import collect_set
    from pyspark.sql.functions import concat_ws
    from pyspark.sql.types import StringType, ArrayType
    import nltk
    from nltk.stem import WordNetLemmatizer
    from pyspark.sql.functions import concat_ws, collect_set, trim, collect_list, count, regexp_replace, col, lit, lower,length,max
    import re
    par_chd_relations = False
    no_relations = False
    suffix = "_all_relations_final"
    def get_headers(filename):
        if filename == "mrconso":
            return ["CUI","LAT","TS","LUI","STT","SUI","ISPREF","AUI","SAUI","SCUI","SDUI","SAB","TTY","CODE","STR","SRL","SUPPRESS","CVF","_LAST"]
        if filename == "mrsat":
            return ["CUI","LUI","SUI","METAUI","STYPE","CODE","ATUI","SATUI","ATN","SAB","ATV","SUPPRESS","CVF","_LAST"]
        if filename == "mrdef":
            return ["CUI","AUI","ATUI","SATUI","SAB","DEF","SUPPRESS","CVF","_LAST"]
        if filename == "mrsty":
            return ["CUI","TUI","STN","STY","ATUI","CVF","_LAST"]
        if filename == "mrrel":
            return ["CUI1","AUI1","STYPE1","REL","CUI2","AUI2","STYPE2","RELA","RUI","SRUI","SAB","SL","RG","DIR","SUPPRESS","CVF","_LAST"]
        if filename == "mrhier":
            return ["CUI","AUI","CXN","PAUI","SAB","RELA","PTR","HCD","CVF","_LAST"]
        return [""]
    hadoop_base_path = 's3://emrlinkedbucket/hdfsData/app-data/'
    dfconso = spark.read.format("csv").option("delimiter", "|").load(f"{hadoop_base_path}/umls/MRCONSO.RRF").toDF(*get_headers("mrconso")).drop("_LAST")
    dfconso_temp = dfconso.filter(col("LAT")=="ENG")
    dfconso_temp = dfconso_temp.select("CUI","STR").distinct()
    dfconso_temp = dfconso_temp.withColumn("len",length("STR"))
    dfconso_temp = dfconso_temp.withColumn("STR", regexp_replace(col("STR"), r"<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});", " ")).withColumn("STR", regexp_replace(col("STR"), r"[',\";()\[\]]", " ")).withColumn("STR", regexp_replace(col("STR"), r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?������]))", " "))
    dfconso_temp_len = dfconso_temp.groupBy("CUI").agg(max("len").alias("len"))
    dfconso_temp = dfconso_temp.join(dfconso_temp_len,["CUI","len"],"inner")
    dfconso_temp = dfconso_temp.dropDuplicates(["CUI","len"])
    dfdef = spark.read.format("csv").option("delimiter", "|").load(f"{hadoop_base_path}/umls/MRDEF.RRF").toDF(*get_headers("mrdef")).drop("_LAST")
    sabvals = ["MSHPOR","MSHCZE","MSHSPA","MSHSWE","MSHNOR","SCTSPA","MDRDUT","MDRGER","MDRITA","MDRSPA","SCTSPA","MDRFRE","MDRPOR","MDRCZE","MDRJPN","MDRHUN","MDRSPA","MDRRUS","MDRKOR","MDRBPO"]
    dfdef = dfdef.fillna("MSH",subset=["SAB"])
    dfdef = dfdef.filter(~col("SAB").isin(*sabvals)).withColumn("DEF", regexp_replace(col("DEF"), r"<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});", " ")).withColumn("DEF", regexp_replace(col("DEF"), r"[',\";()\[\]]", " ")).withColumn("DEF", regexp_replace(col("DEF"), r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?������]))", " ")).select("CUI", "DEF").distinct()    
    dfdef = dfdef.groupBy("CUI").agg(concat_ws(' ', collect_set("DEF")).alias("DEF"))
    dfdef = dfdef.join(dfconso_temp,["CUI"],"right")
    dfdef = dfdef.withColumn("DEF_with_NAME",concat_ws('.',col("STR"),col("DEF")))
    dfdef = dfdef.drop("DEF")
    dfdef = dfdef.withColumnRenamed("DEF_with_NAME","DEF")
    dfdef = dfdef.withColumn("DEF", regexp_replace(col("DEF"), r"[^\sa-zA-Z0-9]", " "))
    dfrel = spark.read.format("csv").option("delimiter", "|").load(f"{hadoop_base_path}/umls/MRREL.RRF").toDF(*get_headers("mrrel")).drop("_LAST")
    dfconso = dfconso.select("CUI").distinct()
    if par_chd_relations:
        dfrel = dfrel.filter(col("CUI1").isNotNull() & col("CUI2").isNotNull()).filter(~col("REL").isin("AQ", "QB", "RB", "RL", "RN", "RO", "RQ", "RU", "SY", "DEL", "XR", "")).filter(col("CUI1") != col("CUI2")).select("CUI1", "CUI2").withColumnRenamed("CUI1", "CUI").withColumnRenamed("CUI2", "RCUI").distinct()
    else:
        dfrel = dfrel.filter(col("CUI1").isNotNull() & col("CUI2").isNotNull()).filter(~col("REL").isin("DEL", "XR", "")).filter(col("CUI1") != col("CUI2")).select("CUI1", "CUI2").withColumnRenamed("CUI1", "CUI").withColumnRenamed("CUI2", "RCUI").distinct()
    def sent_TokenizeFunct(x):
        return nltk.sent_tokenize(x.lower())
    def word_TokenizeFunct(x):
        splitted = [word for line in x for word in line.split()]
        return splitted
    def removeStopWordsFunct(x):
        from nltk.corpus import stopwords
        stop_words=set(stopwords.words('english'))
        filteredSentence = [w for w in x if not w in stop_words]
        return filteredSentence
    def lemma(x):
        return ' '.join(x)
    ss = "lisinopril\n\n orcid0000000222344248\t\t pmid 29910038 sdns steroiddependent nephrotic syndrome nephrotic syndrome twentyfour hour urine protein > three grams renal nephrotic syndrome"
    measurement_units = ['cm','mg','metre', 'millimeter', 'centimeter', 'decimeter', 'kilometer', 'inch', 'foot', 'yard', 'mile', 'nautical mile', 'square meter', 'acre', 'are', 'hectare', 'square inch', 'square feet', 'square yard', 'square mile', 'cubic meter', 'liter' , 'milliliter', 'centiliter', 'deciliter',   'hectoliter',  'cubic inch',  'cubic foot',  'cubic yard', 'acre-foot', 'teaspoon',  'tablespoon',  'fluid ounce', 'cup', 'gill', 'pint', 'quart', 'gallon', 'radian',  'degree',  'steradian', 'second',  'minute',  'hour', 'day', 'year', 'hertz', 'angular', 'frequency', 'decibel', 'kilogram', 'gram', 'grain',   'dram' , 'ounce',   'pound',   'ton', 'tonne',  'slug', 'density', 'denier',  'tex', 'decitex', 'mommes',  'newton',  'kilopond',  'pond', 'joule' ,  'watt',  'kilowatt',  'horsepower',  'pascal', 'bar', 'pounds', 'kelvin',  'centigrade', 'calorie', 'fahrenheit',  'candela', 'per', 'lumen' ,  'lux', 'diopter', 'ampere',  'coulomb', 'volt' , 'ohm', 'farad', 'siemens', 'henry', 'weber' ,  'tesla', 'becquerel',   'mole', 'paper', 'bale',  'dozen']
    num_words = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven','twelve','thirteen','fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'ninteen',  'twenty', 'thirty', 'forty', 'fifty'  'hundred', 'thousand', 'million']
    def nume(wd):
        fl = False
        for num in num_words:
            if num==wd:
                fl = True
                break
        return fl
    def rm1(str):
        words = str.lower().split()
        words = [i for i in words if not i.isnumeric()]
        words = [i for i in words if i not in measurement_units]
        words = [i for i in words if not nume(i)]
        words = [i for i in words if re.search('[0-9]+', i) is None]
        return " ".join(words)
    clear_udf = udf(rm1, StringType())
    dfdef = dfdef.withColumn("DEF", clear_udf(col("DEF")))
    sent_TokenizeFunct_udf = udf(lambda z: sent_TokenizeFunct(z))
    word_TokenizeFunct_udf = udf(lambda z: word_TokenizeFunct(z))
    removeStopWordsFunct_udf = udf(lambda z: removeStopWordsFunct(z))
    lemma_udf = udf(lambda z: lemma(z))
    def remove_duplicates(text):
        text = text.split('.')
        text = [i.strip() for i in text if i.strip()]
        text = set(text)
        text = " ".join([i + '.' for i in text])
        return text.strip()
    dfdef = dfdef.withColumn("DEF", sent_TokenizeFunct_udf(col("DEF"))).withColumn("DEF", word_TokenizeFunct_udf(col("DEF"))).withColumn("DEF", removeStopWordsFunct_udf(col("DEF"))).withColumn("DEF", lemma_udf(col("DEF")))
    if no_relations:
        dfdef = dfdef.withColumnRenamed("DEF", "TEXT")
        dfdef = dfdef.filter(col("TEXT")!="")
        dfdef.write.mode("overwrite").format("com.databricks.spark.csv").option("header", "true").save(f's3://emrlinkedbucket/hdfsData/app-data/umls/corpus{suffix}.csv')
    else:
        df = dfconso.join(dfrel, ["CUI"], "left").join(dfdef, ["CUI"], "left").join(dfdef.withColumnRenamed("CUI", "RCUI").withColumnRenamed("DEF", "RDEF"), ["RCUI"], "left").distinct()
        df2 = df.groupBy("CUI").agg(concat_ws(' ', collect_set("DEF")).alias("DEF"), concat_ws(' ', collect_set("RDEF")).alias("RDEF"))
        finaldf = df2.withColumn("TEXT", concat_ws(' ', col("DEF"), col("RDEF"))).select("CUI", "TEXT")
        finaldf = finaldf.dropna()
        remove_duplicates_udf = udf(remove_duplicates,StringType())
        finaldf = finaldf.withColumn("TEXT",remove_duplicates_udf("TEXT"))
        finaldf = finaldf.filter(col("TEXT")!="")
        finaldf.write.mode("overwrite").parquet(f's3://emrlinkedbucket/hdfsData/app-data/umls/corpus{suffix}')
        #finaldf.coalesce(1).write.mode("overwrite").format("com.databricks.spark.csv").option("header", "true").save(f's3://emrlinkedbucket/hdfsData/app-data/umls/corpus{suffix}_new.csv')
if True:
    df = spark.read.load(f's3://emrlinkedbucket/hdfsData/app-data/umls/corpus{suffix}')
    df.filter(df.CUI=="C0020541").show(100,False)
