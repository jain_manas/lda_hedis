if True:
    df=spark.read.option("header",True).csv("s3://emrlinkedbucket/hdfsData/app-data/umls/num_codes.csv")
    df = df.withColumnRenamed("Code Description","STR").withColumnRenamed("Code","CODE")
    dfconso = spark.read.format("csv").option("delimiter", "|").load(f"{hadoop_base_path}/umls/MRCONSO.RRF").toDF(*get_headers("mrconso")).drop("_LAST")
    dfconso = dfconso.select("CODE","STR","CUI")
    df = df.join(dfconso,["STR","CODE"],"inner")
    df.coalesce(1).write.mode("overwrite").format("com.databricks.spark.csv").option("header", "true").save(f's3://emrlinkedbucket/hdfsData/app-data/umls/corpus/code_cui_mapping.csv')